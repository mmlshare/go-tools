package httppojo

import "encoding/json"

func Obj2Json(obj interface{}) string {
	json, err := json.Marshal(obj)
	if err != nil {
		Error.Println("Obj2Json error", err.Error())
		return ""
	}
	return string(json)
}

func Json2Obj(jsonStr string, obj interface{}) {
	err := json.Unmarshal([]byte(jsonStr), obj)
	if err != nil {
		Error.Println("Json2Obj error", err.Error())
	}
}
